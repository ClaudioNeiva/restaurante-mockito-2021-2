package br.ucsal._20212.testequalidade.restaurante.enums;

public enum SituacaoMesaEnum {
	LIVRE, OCUPADA;
}
