package br.ucsal._20212.testequalidade.restaurante.domain;

import java.util.HashMap;
import java.util.Map;

import br.ucsal._20212.testequalidade.restaurante.enums.SituacaoComandaEnum;

public class Comanda {

	private static Integer seq = 0;

	private Integer codigo;

	private Mesa mesa;

	private Map<Item, Integer> itens = new HashMap<>();

	private SituacaoComandaEnum situacao = SituacaoComandaEnum.ABERTA;

	public Comanda(Mesa mesa) {
		super();
		codigo = ++seq;
		this.mesa = mesa;
	}

	public void incluirItem(Item item, Integer qtd) {
		Integer qtdAtual = 0;
		if (itens.containsKey(item)) {
			qtdAtual = itens.get(item);
		}
		qtdAtual += qtd;
		itens.put(item, qtdAtual);
	}

	public void setSituacao(SituacaoComandaEnum situacao) {
		this.situacao = situacao;
	}

	public SituacaoComandaEnum getSituacao() {
		return situacao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public Mesa getMesa() {
		return mesa;
	}
	
	public Map<Item, Integer> getItens() {
		return new HashMap<>(itens);
	}

	public Double calcularTotal() {
		Double total = 0d;
		for (Item item : itens.keySet()) {
			total += item.getValorUnitario() * itens.get(item);
		}
		return total;
	}

}
